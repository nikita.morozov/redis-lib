package redisHandler

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"time"
)

type Handlers interface {
	Set(uid uint, sid string, payload []byte) error
	Get(uid uint, sid string) ([]byte, error)
	Clear(uid uint, sid string) error
	List() (*[]string, error)
	Update(uid uint, payload []byte) (bool, error)
}

type redisSessionHandler struct {
	Prefix   string
	Duration time.Duration
	Ctx      context.Context
	Client   *redis.Client
}

func (r *redisSessionHandler) getKey(uid uint, sid string) string {
	return fmt.Sprintf("%s:%d:%s", r.Prefix, uid, sid)
}

func (r *redisSessionHandler) Set(uid uint, sid string, payload []byte) error {
	return r.Client.Set(r.Ctx, r.getKey(uid, sid), payload, r.Duration).Err()
}

func (r *redisSessionHandler) Get(uid uint, sid string) ([]byte, error) {
	res := r.Client.Get(r.Ctx, r.getKey(uid, sid))
	if res.Err() != nil {
		return nil, res.Err()
	}

	return res.Bytes()
}

func (r *redisSessionHandler) Clear(uid uint, sid string) error {
	return r.Client.Del(r.Ctx, r.getKey(uid, sid)).Err()
}

func (r *redisSessionHandler) List() (*[]string, error) {
	list, err := r.Client.Keys(r.Ctx, "*").Result()
	return &list, err
}

func (r *redisSessionHandler) Update(uid uint, payload []byte) (bool, error) {
	list, err := r.Client.Keys(r.Ctx, r.getKey(uid, "*")).Result()
	if err != nil {
		return false, err
	}

	for _, key := range list {
		r.Client.Set(r.Ctx, key, payload, r.Duration)
	}

	return len(list) > 0, nil
}

func NewRedisSessionHandler(client *redis.Client, duration time.Duration) Handlers {
	return &redisSessionHandler{
		Prefix:   "session",
		Duration: duration,
		Ctx:      context.Background(),
		Client:   client,
	}
}
